package com;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class BeerSupportApplication {
	public static void main(String[] args) {
		SpringApplication.run(BeerSupportApplication.class, args);
	}
}