package com.beer;

import com.beer.model.*;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Set;

@Service
public class BeerService {
    private BeerDAO beerDAO;

    public BeerService(BeerDAO beerDAO) {
        this.beerDAO = beerDAO;
    }

    public Set<Beer> retrieveAllBeers() {
        return beerDAO.getAllBeers();
    }

    public Set<?> retrieveSpecificBeer(String group) throws NoSuchElementException {
        Set<?> resultBeers;

        switch (group) {
            case "stout":
                resultBeers = beerDAO.getAllStoutBeers();
                break;
            case "lager":
                resultBeers = beerDAO.getAllLagerBeers();
                break;
            case "trappiste":
                resultBeers = beerDAO.getAllTrappisteBeers();
                break;
            case "blonde":
                resultBeers = beerDAO.getAllBlondeBeers();
                break;
            default:
                throw new NoSuchElementException("The group of beers " + group + " was not found");
        }
        return resultBeers;
    }

    public Beer findBeerByBrandAndName(String brand, String name) throws NoSuchElementException {
        return beerDAO.getBeerByBrandAndName(brand, name);
    }
}
