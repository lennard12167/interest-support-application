package com.beer;

import com.beer.enums.Currency;
import com.beer.model.*;
import com.beer.enums.Type;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

@Component
public class BeerDAO {
    private final StoutBeer stoutBeer1 = new StoutBeer(Type.STOUTBEER, "Guiness", "Draught", "https://cdn.iconscout.com/icon/free/png-512/guinness-1-283284.png", 4.3);
    private final StoutBeer stoutBeer2 = new StoutBeer(Type.STOUTBEER, "Emelisse", "Imperial Russian Stout", "https://brewerydb-images.s3.amazonaws.com/brewery/FR3XyG/upload_SN6EMT-squareLarge.png", 11.0);

    private final LagerBeer lagerBeer1 = new LagerBeer(Type.LAGERBEER, "Heineken", "Pilsener", "https://cdn.iconscout.com/icon/free/png-512/heineken-4-282265.png", 5.0);
    private final LagerBeer lagerBeer2 = new LagerBeer(Type.LAGERBEER, "Hertog Jan", "Pilsener", "https://untappd.akamaized.net/site/beer_logos_hd/beer-94130_a562f_hd.jpeg", 5.1);

    private final TrappisteBeer trappisteBeer1 = new TrappisteBeer(Type.TRAPPISTEBEER, "LaTrappe", "Double", "https://www.cantinadellabirra.it/media/wysiwyg/loghibirrifici/la-trappe.jpg", 7.0, true, true);
    private final TrappisteBeer trappisteBeer2 = new TrappisteBeer(Type.TRAPPISTEBEER, "LaTrappe", "Quadruple", "https://www.cantinadellabirra.it/media/wysiwyg/loghibirrifici/la-trappe.jpg", 10.0, true, true);
    private final TrappisteBeer trappisteBeer3 = new TrappisteBeer(Type.TRAPPISTEBEER, "Westmalle", "Tripel", "https://brewerydb-images.s3.amazonaws.com/brewery/vLLvs6/upload_jrK9CH-squareLarge.png", 9.5, true, false);

    private final BlondeBeer blondeBeer1 = new BlondeBeer(Type.BLONDEBEER, "Duvel", "Blond",  "https://pbs.twimg.com/profile_images/798924269782794241/OwK05VjL.jpg", 8.5, false);

    private final Set<Beer> allBeers = new HashSet<>();
    private final Set<StoutBeer> stoutBeers = new HashSet<>();
    private final Set<LagerBeer> lagerBeers = new HashSet<>();
    private final Set<TrappisteBeer> trappisteBeers = new HashSet<>();
    private final Set<BlondeBeer> blondeBeers = new HashSet<>();

    public BeerDAO() {

        fillStoutBeersSet();
        fillLagerBeersSet();
        fillTrappisteBeersSet();
        fillblondeBeersSet();

        addAllBeerstoAllSets();
    }

    private void addAllBeerstoAllSets() {
        allBeers.addAll(stoutBeers);
        allBeers.addAll(lagerBeers);
        allBeers.addAll(trappisteBeers);
        allBeers.addAll(blondeBeers);
    }

    private void fillStoutBeersSet() {
        setPriceStoutBeers();

        stoutBeers.add(stoutBeer1);
        stoutBeers.add(stoutBeer2);
    }

    private void fillLagerBeersSet() {
        setPriceLagerBeers();

        lagerBeers.add(lagerBeer1);
        lagerBeers.add(lagerBeer2);
    }

    private void fillTrappisteBeersSet() {
        setPriceTrappisteBeers();

        trappisteBeers.add(trappisteBeer1);
        trappisteBeers.add(trappisteBeer2);
        trappisteBeers.add(trappisteBeer3);
    }

    private void fillblondeBeersSet() {
        setPriceBlondeBeers();

        blondeBeers.add(blondeBeer1);
    }

    private void setPriceStoutBeers() {
        stoutBeer1.addToPriceMap(Currency.EUR, 2.70);
        stoutBeer1.addToPriceMap(Currency.USD, 3.10);
        stoutBeer1.addToPriceMap(Currency.GBP, 2.00);

        stoutBeer2.addToPriceMap(Currency.EUR, 3.10);
        stoutBeer2.addToPriceMap(Currency.USD, 4.30);
        stoutBeer2.addToPriceMap(Currency.GBP, 3.15);
    }

    private void setPriceLagerBeers() {
        lagerBeer1.addToPriceMap(Currency.EUR, 1.00);
        lagerBeer1.addToPriceMap(Currency.USD, 1.10);
        lagerBeer1.addToPriceMap(Currency.GBP, 1.40);

        lagerBeer2.addToPriceMap(Currency.EUR, 1.10);
        lagerBeer2.addToPriceMap(Currency.USD, 1.65);
    }

    private void setPriceTrappisteBeers() {
        trappisteBeer1.addToPriceMap(Currency.EUR, 2.70);
        trappisteBeer1.addToPriceMap(Currency.GBP, 1.70);

        trappisteBeer2.addToPriceMap(Currency.EUR, 2.05);
        trappisteBeer2.addToPriceMap(Currency.GBP, 2.15);

        trappisteBeer3.addToPriceMap(Currency.EUR, 2.15);
        trappisteBeer3.addToPriceMap(Currency.USD, 3.95);
        trappisteBeer3.addToPriceMap(Currency.GBP, 2.40);
    }

    private void setPriceBlondeBeers() {
        blondeBeer1.addToPriceMap(Currency.EUR, 1.45);
        blondeBeer1.addToPriceMap(Currency.USD, 2.00);
        blondeBeer1.addToPriceMap(Currency.GBP, 1.25);
    }

    public Set<Beer> getAllBeers() {
        return allBeers;
    }

    public Set<StoutBeer> getAllStoutBeers() {
        return stoutBeers;
    }
    public Set<LagerBeer> getAllLagerBeers() {
        return lagerBeers;
    }
    public Set<TrappisteBeer> getAllTrappisteBeers() {
        return trappisteBeers;
    }
    public Set<BlondeBeer> getAllBlondeBeers() {
        return blondeBeers;
    }

    public Beer getBeerByBrandAndName(String brand, String name) throws NoSuchElementException {
        for(Beer beer : allBeers) {
            if(beer.getBrand().equals(brand) && beer.getName().equals(name))
                return beer;
        }
        throw new NoSuchElementException("Specified beer with brand " + brand + " and name " + name + " was not found");
    }
}
