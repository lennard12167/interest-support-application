package com.beer.enums;

public enum Type {
    BLONDEBEER,
    LAGERBEER,
    STOUTBEER,
    TRAPPISTEBEER
}
