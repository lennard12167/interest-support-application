package com.beer.enums;

public enum Currency {
    EUR("€"),
    USD("$"),
    GBP("₤");

    public final String sign;

    Currency(String sign) {
        this.sign = sign;
    }
}
