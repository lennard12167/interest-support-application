package com.beer.model;

import com.beer.enums.Type;

public class TrappisteBeer extends Beer {
    private final boolean monastery;
    private final boolean darkBeer;

    public TrappisteBeer(Type type, String brand, String name, String imageUri, double alcoholPercentage, boolean monastery, boolean darkBeer) {
        super(type, brand, name, imageUri, alcoholPercentage);
        this.monastery = monastery;
        this.darkBeer = darkBeer;
    }

    public boolean isMonastery() {
        return monastery;
    }

    public boolean isDarkBeer() {
        return darkBeer;
    }
}
