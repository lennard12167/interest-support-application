package com.beer.model;

import com.beer.enums.Type;

public class BlondeBeer extends Beer {
    private final boolean monastery;

    public BlondeBeer(Type type, String brand, String name, String imageUri, double alcoholPercentage, boolean monastery) {
        super(type, brand, name, imageUri, alcoholPercentage);
        this.monastery = monastery;
    }

    public boolean getMonastery() {
        return monastery;
    }
}
