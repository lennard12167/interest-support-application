package com.beer.model;


import com.beer.enums.Currency;
import com.beer.enums.Type;

import java.util.HashMap;
import java.util.Map;

public abstract class Beer {
    protected final Type type;
    protected final String brand;
    protected final String name;
    protected final String imageUri;
    protected final double alcoholPercentage;
    protected final Map<Currency, Double> priceMap = new HashMap<>();

    public Beer(Type type, String brand, String name, String imageUri, double alcoholPercentage) {
        this.type = type;
        this.brand = brand;
        this.name = name;
        this.imageUri = imageUri;
        this.alcoholPercentage = alcoholPercentage;
    }

    public void addToPriceMap(Currency currency, Double price) {
        priceMap.put(currency, price);
    }

    public Type getType() {
        return type;
    }

    public String getBrand() {
        return brand;
    }

    public String getName() {
        return name;
    }

    public String getImageUri() {
        return imageUri;
    }

    public double getAlcoholPercentage() {
        return alcoholPercentage;
    }

    public Map<Currency, Double> getPriceMap() {
        return priceMap;
    }
}
