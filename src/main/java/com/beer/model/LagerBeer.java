package com.beer.model;

import com.beer.enums.Type;

public class LagerBeer extends Beer{
    public LagerBeer(Type type, String brand, String name, String imageUri, double alcoholPercentage) {
        super(type, brand, name, imageUri, alcoholPercentage);
    }
}
