package com.beer;

import com.beer.model.Beer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;
import java.util.Set;

@RestController
public class BeerRestController {
    private BeerService beerService;

    @Autowired
    public BeerRestController(BeerService beerService) {
        this.beerService = beerService;
    }

    @GetMapping("/beer")
    public ResponseEntity<?> retrieveAllBeers() {
        System.out.println("/beer endpoint was called");
        Set<Beer> beers = beerService.retrieveAllBeers();
        return new ResponseEntity<>(beers, HttpStatus.OK);
    }

    @GetMapping("/beer/{group}")
    public ResponseEntity<?> retrieveSpecificBeers(@PathVariable("group") String group) {
        System.out.println("/beer/{group} endpoint was called");
        group = group == null ? "" : group;

        try {
            Set<?> specificBeers = beerService.retrieveSpecificBeer(group);
            return new ResponseEntity<>(specificBeers, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/beer/{brand}/{name}")
    public ResponseEntity<?> retrieveSinglebeer(@PathVariable("brand") String brand, @PathVariable("name") String name) {
        System.out.println("/beer/{brand}/{name} endpoint was called");
        brand = brand == null ? "" : brand;
        name = name == null ? "" : name;

        try {
            Beer beer = beerService.findBeerByBrandAndName(brand, name);
            return new ResponseEntity<>(beer, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
